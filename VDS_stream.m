%   TCP/IP Verbindung zu IPG Carmaker über VDS
%   Genauere Beschreibung der Datenstruktur in IPG Movie Doku 
clear;
close all;
%localhost wen IPG auf gleichem Rechner läuft
Adress = "localhost";
%Port wird in VDS.cfg gesetzt (default 2210)
port = 2210;            
client_connected = false;
got_image = false;
%TCP Verbindung erstellen
vds_client = tcpclient(Adress, port);  

% Ersten String von Carmaker Empfangen
% bei erfolgreicher Verbindung -> "*IPGMovie 9.1.1 12/16/20\n"
data = read(vds_client,64,"char");
if contains(data, "*IPGMovie")
   disp("IPG Movie is Connected...");
   client_connected = true;
   disp(data);
end

%Empfangen der Bildinformationen + Bild
%Erwartete Struktur -> "*VDS <no> <fmt> <time> <wi>x<he> <len>\n" + Data 
while(got_image == false)
    while(1)
      if client_connected && contains(data, "*VDS")
        got_image = true;
        % Daten String emfangen (Länge = Info(max 64) + Daten(w*h*Layer)
        img_data = read(vds_client,120000,"uint8");
        meta_data = read(vds_client,64,"char");
        
        %disp(meta_data);
        %disp(img_data);
        %Daten aufteilen über Leerzeichen
        splitdata = strsplit(meta_data," ");
        
        %imgtype = splitdata(3);
        timestamp = splitdata(4);
        disp(timestamp);
        %img_size = splitdata(5);
        %data_len = splitdata(6);
        %img_size_split = split(img_size,"x");
        %image_w = img_size_split(1);
        %imag_h = img_size_split(2);

        %Extrahierten der Bilddaten
        %img = extractBefore(splitdata{1,1}, "*VDS");
        %Reshape in Matrix
        img_matrix = reshape(img_data,[200,200,3]);
        % Konvertierten zu Double
        %pic = double(img_matrix);
        img_matrix_rgb = uint8.empty(200,200,0);
        img_matrix_rgb(:,:,1) = reshape(img_data(1:3:end),[200,200])';
        img_matrix_rgb(:,:,2) = reshape(img_data(2:3:end),[200,200])';
        img_matrix_rgb(:,:,3) = reshape(img_data(3:3:end),[200,200])';
        %Bild ausgeben
        %imshow(img_matrix_rgb);
        detectLanes(img_matrix_rgb,200,200);
    else 
        pause(0.1);
        data = read(vds_client,64,"char");
      end
    end
end

